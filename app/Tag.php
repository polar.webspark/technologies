<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    public $timestamps = false;

    public function definitions()
    {
        return $this->belongsToMany('App\Definition');
    }
}
