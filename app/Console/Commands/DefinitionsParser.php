<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\CustomClasses\TechnologiesParser;

class DefinitionsParser extends Command
{
    const TERMS_URL = 'https://www.techopedia.com/it-terms/';

    protected $signature = 'parseDefinitions';

    protected $description = 'Parse definitions';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $alphabet = range('a','z');
        array_unshift($alphabet,'1');
        foreach ($alphabet as $letter) {
            $terms = TechnologiesParser::parseTerms(self::TERMS_URL . $letter);
            sleep(1);
            $this->info(self::TERMS_URL . $letter . ' start to parse');
            for ($i = 0, $termsCount = sizeof($terms['url']); $i < $termsCount; $i++) {
                $url = $terms['url'][$i];
                TechnologiesParser::saveDefinition($url);
                sleep(1);
            }
            $this->info(self::TERMS_URL . $letter . ' parse completed');
        }
        return 0;
    }
}
