<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\CustomClasses\TechnologiesParser;
use App\Definition;
use App\CustomClasses\Logger;

class TagsParser extends Command
{
    protected $signature = 'parseTags';

    protected $description = 'Parse tags';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $definitions = Definition::all();
        if (count($definitions) > 0) {
            foreach ($definitions as $definition) {
                TechnologiesParser::saveTags($definition);
            }
        }else {
            Logger::setInfo('No definitions in DB');
            $this->error('No definitions in DB');
        }
        return 0;
    }
}
