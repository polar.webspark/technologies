<?php

namespace App\Http\Controllers;

use App\Definition;
use App\Term;
use Illuminate\Http\Request;
use App\CustomClasses\TechnologiesParser;

class DefinitionController extends Controller
{
    public function index()
    {
        $definitions = Definition::orderBy('title')->get();
        return view('terms', compact('definitions'));
    }

    public function show($id)
    {
        $definition = Definition::findOrFail($id);
        return view('term', compact('definition'));
    }
}



