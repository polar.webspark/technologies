<?php

namespace App\CustomClasses;

use App\Definition;
use App\Tag;
use App\CustomClasses\Logger;

class TechnologiesParser
{
    public static function getPage($url, $folder)
    {
        $url = urldecode($url);
        $url = htmlspecialchars_decode($url);
        $url = str_replace(" ", "%20", $url);
        $filename = "app/technologiesPages/" . $folder . "/" . md5($url) . ".html";
        if (file_exists($filename)) {
            if(time() - filemtime($filename) < 24*10*60*60) {
                $data = file_get_contents($filename);
                return $data;
            }
        }
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        $data = curl_exec($ch);
        if ($data === false) {
            Logger::setInfo("Ошибка CURL: ".curl_error($ch));
            return "Ошибка CURL: ".curl_error($ch);
        }
        curl_close($ch);
        file_put_contents($filename, $data);
        return $data;
    }

    public static function parseTerms($url)
    {
        $data = self::getPage($url, 'terms');
        preg_match('#<div class="firstColumn">.*<div class="secondColumn visible-lg visible-md">#isu', $data, $termsList);
        preg_match_all('#<a href="(?<url>[^>]+)">(?:.+?)</a>#isu', $termsList[0], $terms);
        return $terms;
    }

    public static function parseDefinition($url)
    {
        $data = self::getPage($url, 'definitions');
        preg_match('#<h1>[^>]*<span itemprop="name headline">(.+?)</span>[^>]*</h1>#isu', $data, $title);
        preg_match('#<div itemprop="description">(.*?)<\/div>#isu', $data, $meaning);
        preg_match('#<h2>Techopedia explains .*?</h2>.*?<div class="floatingSponsor">.*?<div>(.*?)</div>.*?</div>#isu', $data, $explains);
        return ['title' => $title[1], 'meaning' => $meaning[1], 'explains' => $explains[1]];
    }

    public static function parseRelatedTerms($url)
    {
        $data = self::getPage($url, 'definitions');
        preg_match('#<div id="divRelatedTerm" class="related-terms-left-col">.+?<ul>(.+?)</ul>.+?</div>#isu', $data, $relatedTermsList);
        preg_match_all('#<li>.*?<a [^>]+ href="([^>]+)">\s*(.+?)\s*</a>.*?</li>#isu', $relatedTermsList[1], $relatedTerms);
        return ['Urls' => $relatedTerms[1], 'Names' => $relatedTerms[2]];
    }

    public static function parseTags($url)
    {
        $data = self::getPage($url, 'definitions');
        preg_match('#<a href="/dictionary/tags">Tags</a>(.*?)<div class="term-item-wrapper" itemscope itemtype="http://schema\.org/Article">#isu', $data, $primaryTagsList);
        preg_match_all('#<a href="(?:[^>]+)">(?<name>.*?)</a>#isu', $primaryTagsList[1], $primaryTags);
        preg_match('#<div id="divTags" class="tagbutton">(.*?)</div>#isu', $data, $fullTagsList);
        preg_match_all('#<a class=\'tagbutton\' href="(?:[^"]+)".*?>(?<name>.*?)</a>#isu', $fullTagsList[1], $fullTags);
        return ['primaryTags' => $primaryTags, 'fullTags' => $fullTags];
    }

    public static function saveDefinition($url)
    {
        $definitionsUrl = 'https://www.techopedia.com' . $url;
        $definition = self::parseDefinition($definitionsUrl);
        if(!Definition::where('url', $definitionsUrl)->exists()) {
            $newDefinition = new Definition();
            $newDefinition->title = trim($definition['title']);
            $newDefinition->meaning = trim($definition['meaning']);
            $newDefinition->explains = trim($definition['explains']);
            $newDefinition->url = trim($definitionsUrl);
            if($newDefinition->save()) {
                Logger::setInfo('https://www.techopedia.com'. $url . ' saved to DB');
                return true;
            } else {
                Logger::setInfo('Error when saving https://www.techopedia.com'. $url);
                return false;
            }

        } else {
            $currentDefinition = Definition::where('url', $definitionsUrl)->first();
            $currentDefinition->title = trim($definition['title']);
            $currentDefinition->meaning = trim($definition['meaning']);
            $currentDefinition->explains = trim($definition['explains']);
            if($currentDefinition->save()) {
                Logger::setInfo('https://www.techopedia.com'. $url . ' was update');
                return true;
            } else {
                Logger::setInfo('Error when updating https://www.techopedia.com'. $url);
                return false;
            }
        }
    }

    public static function saveRelatedTerms($definition)
    {
        $relatedTermsIds = array();
        $relatedTerms = self::parseRelatedTerms($definition->url);
        for ($i = 0, $relatedTermsCount = sizeof($relatedTerms['Urls']); $i < $relatedTermsCount; $i++) {
            if (Definition::where('url', 'https://www.techopedia.com' . $relatedTerms['Urls'][$i])->exists()) {
                $relatedDefinition = Definition::where('url', 'https://www.techopedia.com' . $relatedTerms['Urls'][$i])->first();
                array_push($relatedTermsIds, $relatedDefinition->id);
            }
        }
        $definition->relatedTerms()->sync($relatedTermsIds);
        Logger::setInfo(count($relatedTermsIds)." related terms for ".$definition->url." successfully added");
        return true;
    }

    public static function saveTags($definition)
    {
        $tagsIds = array();
        $tags = self::parseTags($definition->url);
        foreach ($tags['fullTags']['name'] as $tagName) {
            if (!Tag::where('name', $tagName)->exists()) {
                $tag = new Tag();
                $tag->name = $tagName;
                $tag->save();
            } else {
                $tag = Tag::where('name', $tagName)->first();
            }
            if (in_array($tagName, $tags['primaryTags']['name'])) {
                $tagsIds [$tag->id] = ['primary' => '1'];
            } else {
                $tagsIds [$tag->id] = ['primary' => '0'];
            }
        }
        $definition->tags()->sync($tagsIds);
        Logger::setInfo(count($tagsIds)." tags for ".$definition->url." successfully added");
        return true;
    }
}