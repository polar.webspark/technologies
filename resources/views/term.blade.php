@extends('layouts.app')

@section('content')
    <h1>{{$definition->title}}</h1>
    <hr>
    <h2>Url</h2>
    <p>{{$definition->url}}</p>

    <hr>
    <h2>Meaning</h2>
    <p>{!! $definition->meaning !!}</p>

    <hr>
    <h2>Explains</h2>
    <p>{!! $definition->explains !!}</p>

    <hr>
    <h2>Related terms</h2>
    <ul>
        @foreach($definition->relatedTerms as $relatedTerm)
            <li>
                <a href="/terms/{{$relatedTerm->id}}">{{$relatedTerm->title}}</a>
            </li>
        @endforeach
    </ul>

    <hr>
    <h2>Primary tags</h2>
    <ul>
        @foreach($definition->primaryTags as $tag)
            <li>
                {{$tag->name}}
            </li>
        @endforeach
    </ul>

    <hr>
    <h2>All tags</h2>
    <ul>
        @foreach($definition->allTags as $tag)
            <li>
                {{$tag->name}}
            </li>
        @endforeach
    </ul>

@endsection