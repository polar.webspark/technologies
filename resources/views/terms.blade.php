@extends('layouts.app')

@section('content')
    <h1>Technologies terms</h1>
    <ul class="list-group">
        @foreach($definitions as $definition)
            <li class="list-group-item">
                <a href="/terms/{{$definition->id}}">{{$definition->title}}</a>
            </li>
        @endforeach
    </ul>
@endsection
